import React, { Component } from "react";
import { connect } from "react-redux";

import userData from "../../data/users_data.js";
import Home from "../Home";
import Create from "../Create";
import Edit from "../Edit";
import * as actions from "../../actions";

class App extends Component {
  componentDidMount() { 
    fetch('http://localhost:8080/users')
    .then(r =>  r.json().then(data => ({status: r.status, body: data})))
    .then( result => {
      console.log('result ', result)
      result.body.users.forEach(user => {
        user.id = user._id;
        this.props.dispatch(actions.addUser(user));
      });
    })
    
  }

  /*
  class App extends Component {
    componentDidMount() {
      userData.forEach(user=> {
        this.props.dispatch(actions.addUser(user));
      })
    }
  }
  */

  render() {
    const { flag } = this.props;
    return (
      <div>
        {flag.route === "home" && <Home />}
        {flag.route === "create" && <Create />}
        {flag.route === "edit" && <Edit data={flag.id} />}
      </div>
    );
  }
}

/*
render(){
  const {flag} = this.props;
  return (
    <div>
    {flag.route === "home" && <Home />}
    {flag.route === "create" && <Create />}
    {flag.route === "edit" && <Edit data={flag.id} />}
  )
}
*/

const mapStateToProps = state => {
  return {
    users: state.users, 
    flag: state.flags
  };
};

export default connect(mapStateToProps)(App);


/*
const mapStateToProps = state => {
  return {
    users: state.users,
    flag: state.flags
  }
}

export default connect(mapStateToProps)(App);
*/