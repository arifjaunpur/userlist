const express = require('express');
const cors = require('cors')
const bodyParser = require('body-parser');
const MongoClient = require('mongodb').MongoClient;

const config = require('./config');
const User = require('./user');

const app = express();

app.use(cors());
app.use(bodyParser.json());

app.use((req, res, next) => {
	const date = new Date()
	let today = date.getFullYear() + '-' + (date.getMonth() + 1) + '-' + date.getDate() + ' ' 
	today += date.getHours() > 9 ? date.getHours() : '0' + date.getHours();
	today += ':';
	today += date.getMinutes() > 9 ? date.getMinutes() : '0' + date.getMinutes();
	today += ':';
	today += date.getSeconds() > 9 ? date.getSeconds() : '0' + date.getSeconds();
	console.log(`${today}  ${req.method}  ${req.url}`);
	next();
})

app.get('/users', async (req, res, next) => {
	
	try {
		const user = new User();
		const result = await user.find();
		user.close();
		res.json({users: result});
	} catch(error) {
		next(error);
	}
});

app.get('/users/:id', async (req, res, next) => {
	try {
		const user = new User();
		const result = await user.findById(req.params.id);
		user.close();
		res.json({user: result});
	} catch(error) {
		next(error);
	}
});

app.post('/users', async (req, res, next) => {
	try {
		const user = new User();
		const result = await user.create(req.body);
		user.close();
		res.json({message: 'User created successfully!', user: result.ops[0]});
	} catch(error) {
		next(error);
	}
});

app.put('/users/:id', async (req, res, next) => {
	try {
		const user = new User();
		const result = await user.updateById(req.params.id, req.body);
		user.close();
		res.json({message: 'User updated successfully!'});
	} catch(error) {
		next(error);
	}
});

app.delete('/users/:id', async (req, res, next) => {
	try {
		const user = new User();
		const result = await user.deleteById(req.params.id);
		user.close();
		res.json({message: 'User deleted successfully!'});
	} catch(error) {
		next(error);
	}
});

// not found
app.use(function(req, res, next) {
	const error = new Error('Not found');
	error.status = 404;
	next(error);
});

// error handler
app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  res.json({message: err.message, error: err});
});

app.listen(config.server.port, () => {
	console.log(`API running on ${config.server.host}:${config.server.port}`);
})
