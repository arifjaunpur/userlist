const mongodb = require('mongodb');
const config = require('./config');

const MongoClient = mongodb.MongoClient;
const ObjectID = mongodb.ObjectID;

class User {
   
    constructor() {
        this.db = null;
        this.client = null;
        this.userCollectionName = 'user';
    }

    async connect() {
        this.client = new MongoClient(config.db.MONGO_URI, config.db.options);
        await this.client.connect();
        this.db = this.client.db(config.db.MONGO_DB_NAME);
        return this.db;
    }

    getObjectID(id) {
        if(!ObjectID.isValid(id)) {
            throw new Error('Invalid user id');
        }
        return new ObjectID(id);
    }

    async findById(id) {
        id = this.getObjectID(id);
        if (!this.db) {
            await this.connect();
        }
        const collection = this.db.collection(this.userCollectionName);
        return collection.findOne({_id: id});
    }

    async find() {
        if (!this.db) {
            await this.connect();
        }
        const collection = this.db.collection(this.userCollectionName);
        return collection.find().toArray();
    }

    async create(data) {
        if (!this.db) {
            await this.connect();
        }
        const collection = this.db.collection(this.userCollectionName);
        return collection.insertOne(data);
    }

    async updateById(id, data) {
        id = this.getObjectID(id);
        if (!this.db) {
            await this.connect();
        }
        const collection = this.db.collection(this.userCollectionName);
        return collection.updateOne({_id: id}, {$set: data});
    }

    async deleteById(id) {
        id = this.getObjectID(id);
        if (!this.db) {
            await this.connect();
        }
        const collection = this.db.collection(this.userCollectionName);
        return collection.findOneAndDelete({_id: id});
    } 

    async close() {
        this.client.close();
    }
}

module.exports = User;