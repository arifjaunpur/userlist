module.exports = {
	server: {
		port: process.env.PORT || 8080,
		host: process.env.HOST || 'localhost',
	},
	db: {
		MONGO_URI: process.env.MONGO_URI || 'mongodb://arif:mak786mak7@ds235768.mlab.com:35768/ses',
		MONGO_DB_NAME: process.env.MONGO_DB_NAME || 'ses',
		options: {
			useNewUrlParser: true
		}
	}
}